﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RyanLoggingTools
{
    public class ContextLogger
    {
        #region Properties
        public EventLog EventSource { get; set; }
        public FileInfo LogFile { get; set; }
        public DateTime InitTime { get; set; }
        public LogSeverity Verbosity { get; set; } = LogSeverity.Information | LogSeverity.Warning | LogSeverity.Error;

        [Flags]
        public enum LogSeverity
        {
            Information = 4,
            Warning = 2,
            Error = 1
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Create a new ContextLogger instance.
        /// </summary>
        /// <param name="eventLog">The EventLog class to write event logs to</param>
        /// <param name="logFile">The FileInfo class of the file to write log data to</param>
        public ContextLogger(EventLog eventLog, FileInfo logFile)
        {
            this.InitTime = DateTime.Now;
            this.EventSource = eventLog;
            this.LogFile = logFile;

            // Create the new event file
            if (LogFile != null && !LogFile.Exists)
            {
                int errCode = 0;
                string errMsg = null;
                try
                {
                    using (FileStream fs = LogFile.Create())
                    {
                        // Write a welcome message to the log file.
                        string dt = _GetDateTimeString();
                        Byte[] welcome = new UTF8Encoding(true).GetBytes($"--------------------\r\n" +
                            $"INFO New RyanLoggingTools ContextLogger file created - {dt}\r\n" +
                            $"--------------------\r\n\r\n");
                        fs.Write(welcome, 0, welcome.Length);
                    }
                }
                catch (ArgumentNullException)
                {
                    errCode = 1001;
                    errMsg = "The file path provided is null.";
                }
                catch (System.Security.SecurityException ex)
                {
                    errCode = 1002;
                    errMsg = "Security error encountered when attempting to access the file path.\r\n" +
                        $"\tFile path: {logFile.FullName}" +
                        $"\tStack trace: {ex.ToString()}";
                }
                catch (PathTooLongException ex)
                {
                    errCode = 1003;
                    errMsg = "The file path provided is too long.\r\n" +
                        $"\tFile path: {logFile.FullName}" +
                        $"\tStack trace: {ex.ToString()}";
                }
                catch (UnauthorizedAccessException ex)
                {
                    errCode = 1004;
                    errMsg = "Not authorised to access the file path provided.\r\n" +
                        $"\tFile path: {logFile.FullName}" +
                        $"\tStack trace: {ex.ToString()}";
                }
                catch (Exception ex)
                {
                    errCode = 1010;
                    errMsg = "Unconsidered Exception handled.\r\n" +
                        $"\tFile path: {logFile.FullName}" +
                        $"\tStack trace: {ex.ToString()}";
                }
                finally
                {
                    if (errCode > 0) _SubmitLog(errCode, $"Failed to create a new ContextLogger log file with the path provided: {errMsg}", LogSeverity.Error);
                }
            }
        }
        #endregion

        #region Methods 
        /// <summary>
        /// Log a new error event to the configured sources
        /// </summary>
        /// <param name="code">The pre-defined error code</param>
        /// <param name="msg">The specific error message</param>
        public void Error(int code, string msg)
        {
            _SubmitLog(code, msg, LogSeverity.Error);
        }

        /// <summary>
        /// Log a new warning event to the configured sources
        /// </summary>
        /// <param name="code">The pre-defined error code</param>
        /// <param name="msg">The specific error message</param>
        public void Warning(int code, string msg)
        {
            _SubmitLog(code, msg, LogSeverity.Error);
        }

        /// <summary>
        /// Log a new information event to the configured sources
        /// </summary>
        /// <param name="code">The pre-defined error code</param>
        /// <param name="msg">The specific error message</param>
        public void Info(int code, string msg)
        {
            _SubmitLog(code, msg, LogSeverity.Information);
        }
        
        /// <summary>
        /// Create a new log in the configured outputs
        /// </summary>
        /// <param name="code">The pre-defined error code</param>
        /// <param name="msg">The descriptive error message</param>
        /// <param name="severityLevel">The severity of the log event</param>
        private void _SubmitLog(int code, string msg, LogSeverity severityLevel)
        {
            // Check verbosity to see if this should be logged, otherwise ignore it
            if (!_ShouldLog(severityLevel)) return;

            string level = _GetLogMessagePrefix(severityLevel);
            string logTime = _GetDateTimeString();

            // Create logs for log sources that have been set up
            bool hasBeenLogged = false;
            if (EventSource != null)
            {
                _LogToEventSource(code, msg, severityLevel);
                hasBeenLogged = true;
            }

            if (LogFile != null)
            {
                string fullMsg = $"{level} {logTime} - ({code}) {msg}";
                _LogToFile(fullMsg);
                hasBeenLogged = true;
            }

            // Trigger this if the message hasn't been logged anywhere
            if (!hasBeenLogged)
            {
                throw new LogException($"Need to log something but there is no configured output!\r\n{level} {logTime} - ({code}) {msg}");
            }
        }

        /// <summary>
        /// Log to the event viewer using the source specified previously
        /// </summary>
        /// <param name="code">The application event code</param>
        /// <param name="msg">A descriptive log message</param>
        /// <param name="severityLevel">The severity of the log event</param>
        private void _LogToEventSource(int code, string msg, LogSeverity severity)
        {
            EventLogEntryType entryType = (EventLogEntryType)severity;
            EventSource.WriteEntry(msg, entryType, code);
        }

        /// <summary>
        /// Write a message to the log file
        /// </summary>
        /// <param name="msg">The message that should be written to the log file</param>
        private void _LogToFile(string msg)
        {
            using (FileStream fs = LogFile.Open(FileMode.Append, FileAccess.Write))
            {
                Byte[] msgBytes = new UTF8Encoding(true).GetBytes(msg + "\r\n");
                fs.Write(msgBytes, 0, msgBytes.Length);
            }
        }

        /// <summary>
        /// Checks whether an event should be logged based on its severity
        /// </summary>
        /// <param name="severityLevel"></param>
        /// <returns></returns>
        private bool _ShouldLog(LogSeverity severityLevel)
        {
            return (severityLevel & Verbosity) == Verbosity;
        }

        private string _GetDateTimeString()
        {
            return DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
        }

        /// <summary>
        /// Convert a LogSeverityLevel flag to a log prefix
        /// </summary>
        /// <param name="severityLevel">The severity of the log event</param>
        /// <returns>A string to prefix logs with</returns>
        private string _GetLogMessagePrefix(LogSeverity severityLevel)
        {
            if ((severityLevel & LogSeverity.Error) == LogSeverity.Error)
            {
                return "ERROR";
            }
            else if ((severityLevel & LogSeverity.Warning) == LogSeverity.Warning)
            {
                return "WARNING";
            }
            else if ((severityLevel & LogSeverity.Information) == LogSeverity.Information)
            {
                return "DEBUG";
            }
            return "UNDEFINED";
        }
        #endregion
    }
}
